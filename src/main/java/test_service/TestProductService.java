/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.dao.ProductDao;
import com.werapan.databaseproject.model.Product;

/**
 *
 * @author werapan
 */
public class TestProductService {
    public static void main(String[] args) {
        ProductDao pd = new ProductDao();
        Product product = pd.get(4);
        System.out.println(product);
        System.out.println(product.getId() + " " +product.getName() + " " + product.getPrice() + " " + product.getCategory().getName());
    }
}
